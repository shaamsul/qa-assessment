Feature: As a ToDo App user should be able to create a task so I can manage my tasks
Scenario: The user should always see the ‘My Tasks’ link on the NavBar
Given I am on avenue code web site with provided URL
When I click on Singn in to Login
And I click on Singn and Provide User and Password
Then Validate Login Message on Sign in Page
 
Scenario: The user should be able to enter a new task by hitting enter or clicking on the add task button.
Given The user should be able to enter a new task by hitting enter or clicking on the add task button
When The task should require at least three characters so the user can enter it
And The task can’t have more than 250 characters.
Then the task should be appended on the list of created tasks
 

 Scenario: The user should see a button labeled as ‘ Manage Subtasks’
Given This button should have the number of subtasks created for that tasks
When The task should require at least three characters so the user can enter it
And The task can’t have more than 250 characters.
Then the task should be appended on the list of created tasks