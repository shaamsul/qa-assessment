package com.cucumber.MavenCucumberPrototype;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Case2 {
WebDriver driver = null;
	
	@Given("^ The user should be able to enter a new task by hitting enter or clicking on the add task button $s")
	public void shouldLandAnenueCodeWebsite() throws Throwable{
		Assert.assertTrue("Welcome, shamshul!", true);
	}
	@When("^The user should be able to enter a new task by hitting enter or clicking on the add task button$")
	public void shouldClickOnTaskButton() throws Throwable{
		driver.findElement(By.id("add task button")).click();
	}
	@When("^The task should require at least three characters so the user can enter it$")
	public void shouldCreateTaskWithMinOfThreeChar() throws Throwable{
		driver.findElement(By.name("task_Input")).sendKeys("SHA");
	}
	@When("^The task can’t have more than 250 characters$")
	public void shouldCreateTaskWitNotMoreThen250Chars() throws Throwable{
		driver.findElement(By.name("Description")).sendKeys("Lead QA Engineer");
		driver.findElement(By.id("Sign in")).click();
	}

	@Then("^the task should be appended on the list of created tasks$")
	public void shouldValidateMessageOnSiginPage() throws Throwable{
		Assert.assertTrue("SHA", true);
		Assert.assertTrue("Lead QA Engineer", true);
		
	}

}
