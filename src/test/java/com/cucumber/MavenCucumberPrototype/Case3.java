package com.cucumber.MavenCucumberPrototype;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Case3 {
	
WebDriver driver = null;
	
	@Given("^  This button should have the number of subtasks created for that tasks $")
	public void shouldLandAnenueCodeWebsite() throws Throwable{
		driver.findElement(By.id("Manage Sub Task")).click();
	}
	@When("^There should be a form so you can enter the SubTask Description (250characters)$")
	public void shouldClickOnTaskButton() throws Throwable{
		driver.findElement(By.name("task_Input")).sendKeys("Lead Engineer for Automation");
	}
	@When("^SubTask due date (MM/dd/yyyy format)(Pre selected on page )$")
	public void shouldCreateTaskWithMinOfThreeChar() throws Throwable{
		driver.findElement(By.id("Manage Sub Task")).click();
	}
	@When("^The task can’t have more than 250 characters and The user should click on the add button to add a new Subtask$")
	public void shouldCreateTaskWitNotMoreThen250Chars() throws Throwable{
		driver.findElement(By.name("Description")).sendKeys("Lead QA Engineer");
		driver.findElement(By.id("add button")).click();
	}

	@Then("^Subtasks that were added should be appended on the bottom part of the modal dialog$")
	public void shouldValidateMessageOnSiginPage() throws Throwable{
		Assert.assertTrue("Lead Engineer for Automation", true);
		
		
	}

}
