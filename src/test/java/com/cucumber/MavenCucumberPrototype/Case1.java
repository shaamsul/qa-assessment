package com.cucumber.MavenCucumberPrototype;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class Case1 {
	
	
	WebDriver driver = null;
	
	@Given("^ I am on avenue code web site with provided URL $s")
	public void shouldLandAnenueCodeWebsite() throws Throwable{
		driver = new FirefoxDriver();
		driver.navigate().to("http://qa-test.avenuecode.com/users/sign_in");
	}
	@When("^I click on Singn in to Login $")
	public void shouldLoginWithRegiserUser() throws Throwable{
		driver.findElement(By.id("/users/sign_in")).click();
	}
	@When("^I click on Singn and Provide User and Password $")
	public void shouldLoginWithProvidedUSerandPass() throws Throwable{
		driver.findElement(By.name("user_email")).sendKeys("shaamsul@gmail.com");
		driver.findElement(By.name("user_password")).sendKeys("123456789");
		driver.findElement(By.id("Sign in")).click();
	}

	@Then("^Validate Login Message on Sign in Page $")
	public void shouldValidateMessageOnSiginPage() throws Throwable{
		Assert.assertTrue("Welcome, shamshul!", true);
		
	}
}
